import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { Company } from '../models/Company';

@Component({
  selector: 'app-companies-list',
  templateUrl: './companies-list.component.html',
  styleUrls: ['./companies-list.component.css']
})
export class CompaniesListComponent implements OnInit {

  companies: Company[] = [];

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.companyService.getCompanies().subscribe(companies => this.companies = companies);
    // this.companies = [
    //   {
    //     name: 'BMW',
    //     employees: 2345,
    //     logo: ''
    //   }, {
    //     name: 'Telia',
    //     employees: 345,
    //     logo: ''
    //   }
    // ];
  }

}
