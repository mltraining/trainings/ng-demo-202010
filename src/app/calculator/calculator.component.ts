import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  number1: number = null;
  number2: number = null;
  sum: number = null;

  constructor() { }

  ngOnInit(): void {
  }

  doAdd(): void {
    this.sum = this.number1 + this.number2;
  }

  doSubtract(): void {
    this.sum = this.number1 - this.number2;
  }

  doMultiply(): void {
    this.sum = this.number1 * this.number2;
  }

  doDivide(): void {
    this.sum = this.number1 / this.number2;
  }

  doClear(): void {
    this.number1 = null;
    this.number2 = null;
    this.sum = null;
  }
}
